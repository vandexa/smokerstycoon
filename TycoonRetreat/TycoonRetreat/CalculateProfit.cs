﻿using MonopolyTest1.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TycoonRetreat
{
    public static class CalculateProfit
    {
        private static readonly decimal
            HOUSE_PRICE         = 100000m,
            FACTORY_PRICE       = 500000m,
            BASE_HOUSE_PROFIT   = 500m,
            BASE_FACTORY_PROFIT = 3000m;

        public static IEnumerable<TileMeta> Calculate(ReadonlySimulationState state)
        {
            var gameTiles = state.Map.Tiles.ToList();

            var GetNeighbours = GetNeighboursHandler(1);
            var GetCommunity  = GetNeighboursHandler(4);
            var Map = MapHandler(gameTiles);

            return gameTiles
                .AsParallel()
                .Select(t => new Tile
                {
                    X = t.Coordinate.X,
                    Y = t.Coordinate.Y,
                    Type = t.Type,
                    IsOwnProperty = t.IsOwned
                })
                .Where(t => (t.Type == TileType.Factory || t.Type == TileType.House))
                .Select(t =>
                {
                    var neighbours = GetNeighbours(t).Select(Map);
                    var community = GetCommunity(t).Select(Map);
                    var profit = GetProfit(t, neighbours, community);

                    return new TileMeta
                    {
                        Price = t.Type == TileType.House ? HOUSE_PRICE :
                                t.Type == TileType.Factory ? FACTORY_PRICE : 0m,
                        Profit = profit,

                        X = t.X,
                        Y = t.Y,
                        Type = t.Type,
                        IsOwnProperty = t.IsOwnProperty,
                    };
                })
                ;
        }

        private static decimal GetProfit(Tile tile, IEnumerable<Tile> neighbours, IEnumerable<Tile> community)
        {
            if (tile.Type == TileType.House)
            {
                return GetHouseProfit(tile, neighbours, community);
            }

            if (tile.Type == TileType.Factory)
            {
                return GetFactoryProfit(tile, neighbours, community);
            }

            return 0m;
        }

        private static decimal GetFactoryProfit(Tile tile, IEnumerable<Tile> neighbours, IEnumerable<Tile> community)
        {
            var rails          = community.Any(c => c.Type == TileType.RailwayH) ? 1 : 0;
            var water          = community.Any(c => c.Type == TileType.Water) ? 1 : 0;
            var otherFactories = neighbours.Count(n => n.Type == TileType.Factory);

            return BASE_FACTORY_PROFIT * (
                    1
                    + rails * .10m
                    + water * .10m
                    + otherFactories * (otherFactories + 1) * .10m
                );
        }

        private static decimal GetHouseProfit(Tile tile, IEnumerable<Tile> neighbours, IEnumerable<Tile> community)
        {
            var housesWeOwn = neighbours.Count(n => n.Type == TileType.House && n.IsOwnProperty);
            var parks       = neighbours.Count(n => n.Type == TileType.Park);
            var water       = neighbours.Any(n => n.Type == TileType.Water) ? 1 : 0;
            var rails       = neighbours.Any(n => n.Type == TileType.RailwayH) ? 1 : 0;
            var factory     = community.Any(c => c.Type == TileType.Factory) ? 1 : 0;

            return BASE_HOUSE_PROFIT * (
                    1
                    + housesWeOwn * (housesWeOwn + 1) * .05m
                    + parks * .05m
                    + water * .10m
                    - rails * .25m
                    - factory * .10m
                );
        }

        private static Func<Coordinate, Tile> MapHandler(ICollection<ReadonlyTile> allTiles)
        {
            return c =>
            {
                var gameTile = allTiles.First(t => t.Coordinate.X == c.X && t.Coordinate.Y == c.Y);
                return new Tile
                {
                    X = c.X,
                    Y = c.Y,
                    IsOwnProperty = gameTile.IsOwned,
                    Type = gameTile.Type
                };
            };
        }

        private static Func<Coordinate, bool> NotOwn(Coordinate tile)
        {
            return other => !(other.X == tile.X && other.Y == tile.Y);
        }

        private static Func<Coordinate, bool> FitsInTheMap(int minX, int minY, int maxX, int maxY)
        {
            return coo =>
                coo.X <= maxX &&
                coo.X >= minX &&
                coo.Y <= maxY &&
                coo.Y >= minY;
        }

        private static Func<Coordinate, IEnumerable<Coordinate>> GetNeighboursHandler(int distance)
        {
            return tile =>
            {
                var yValues = Enumerable.Range(tile.Y - distance, 1 + distance * 2).ToArray();
                var xValues = Enumerable.Range(tile.X - distance, 1 + distance * 2).ToArray();

                var results = new List<Coordinate>();

                for (int i = 0; i < yValues.Length; i++)
                {
                    int y = yValues[i];

                    for (int j = 0; j < xValues.Length; j++)
                    {
                        int x = xValues[j];

                        results.Add(new Coordinate { Y = y, X = x });
                    }
                }

                return results
                    .Where(FitsInTheMap(0, 0, 24, 24))
                    .Where(NotOwn(tile))
                    ;
            };
        }
    }
}
