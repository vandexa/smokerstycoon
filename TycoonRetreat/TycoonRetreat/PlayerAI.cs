﻿using MonopolyTest1.Contracts;
using MonopolyTest1.Contracts.Commands;
using System.Linq;

namespace TycoonRetreat
{
    public class PlayerAI : IPlayerAI
    {
        // goon, fornicator, saturn
        public string LevelPassword { get; set; } = "goon";
        public string CompanyName { get; set; } = "The smokers";
        public IPlayerCommand GetAIAction(ReadonlySimulationState state)
        {
            if (state.Clock.CurrentGameDate == state.Clock.GameEndDate.AddMonths(-1))
            {
                //breakpoint 'cause crash on next turn!!!
            }

            var mostProfitablePropertyWeCanBuy =
                CalculateProfit.Calculate(state)
                    .Where(t => !t.IsOwnProperty)
                    .Where(t => t.Price < state.BankBalance)
                    .OrderByDescending(t => t.Profit)
                    .FirstOrDefault()
                ;


            if (mostProfitablePropertyWeCanBuy == null)
            {
                return new DoNothingCommand();
            }

            return new BuyCommand(new MapCoordinate(mostProfitablePropertyWeCanBuy.X, mostProfitablePropertyWeCanBuy.Y));
        }
    }
}
