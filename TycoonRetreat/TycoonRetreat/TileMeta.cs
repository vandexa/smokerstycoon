﻿using MonopolyTest1.Contracts;
using System;

namespace TycoonRetreat
{

    public class Coordinate : IEquatable<Coordinate>
    {
        public int X { get; set; }
        public int Y { get; set; }

        public bool Equals(Coordinate other)
        {
            if (other == null)
            {
                return false;
            }
            return X == other.X && Y == other.Y;
        }
    }

    public class Tile : Coordinate
    {
        public TileType Type { get; set; }
        public bool IsOwnProperty { get; set; }
        public override string ToString()
        {
            return string.Format("({0},{1})", X, Y);
        }
    }
    public class TileMeta : Tile
    {
        public decimal Price { get; internal set; }
        public decimal Profit { get; internal set; }
    }
}